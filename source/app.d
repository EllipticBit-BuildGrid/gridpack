import std.file;
import std.path;
import std.stdio;
import std.string;

import gridpack.model;
import gridpack.options;
import gridpack.commands.pack;
import gridpack.commands.deploy;

void main(string[] args)
{
    string command = args[1];

    if (command.toLower() == "deploy") {
        writeln("Deploy Specification File: " ~ args[2]);
        DeployOptions opts = new DeployOptions(args[3..$]);
        string specPath = buildNormalizedPath(getcwd(), args[2]);
        Deploy(opts, specPath);
    }

}
