module gridpack.apis;

import std.conv;
import std.datetime;
import std.file;
import std.path;
import std.process;
import std.string;
import std.stdio;
import std.utf;

import gridpack.utils;

version (Windows) {
private import core.sys.windows.com;
private import core.sys.windows.uuid;
private import core.sys.windows.objidl;
private import core.sys.windows.shlobj;

extern(C):
    //Types
    alias HANDLE = void*;
    alias HRESULT = int;
    alias DWORD = int;

    //Registry
    alias HKEY = HANDLE;

    private enum KEY_QUERY_VALUE        = 0x0001;
    private enum KEY_SET_VALUE          = 0x0002;
    private enum KEY_CREATE_SUB_KEY     = 0x0004;
    private enum KEY_ENUMERATE_SUB_KEYS = 0x0008;
    private enum KEY_NOTIFY             = 0x0010;
    private enum KEY_CREATE_LINK        = 0x0020;

    private enum KEY_READ = 0x20019;
    private enum KEY_WRITE = 0x20006;

    private enum HKEY_CURRENT_USER = 0x80000001;
    private enum HKEY_LOCAL_MACHINE = 0x80000002;

    private enum REG_CREATED_NEW_KEY = 0x00000001L;   // New Registry Key created
    private enum REG_OPENED_EXISTING_KEY = 0x00000002L;   // Existing Key opened

    private enum REG_NONE                       = 0U; // No value type
    private enum REG_SZ                         = 1U; // Unicode nul terminated string
    private enum REG_EXPAND_SZ                  = 2U; // Unicode nul terminated string (with environment variable references)
    private enum REG_BINARY                     = 3U; // Free form binary
    private enum REG_DWORD                      = 4U; // 32-bit number
    private enum REG_DWORD_LITTLE_ENDIAN        = 4U; // 32-bit number (same as REG_DWORD)
    private enum REG_DWORD_BIG_ENDIAN           = 5U; // 32-bit number
    private enum REG_LINK                       = 6U; // Symbolic Link (unicode)
    private enum REG_MULTI_SZ                   = 7U; // Multiple Unicode strings
    private enum REG_RESOURCE_LIST              = 8U; // Resource list in the resource map
    private enum REG_FULL_RESOURCE_DESCRIPTOR   = 9U; // Resource list in the hardware description
    private enum REG_RESOURCE_REQUIREMENTS_LIST = 10U;
    private enum REG_QWORD                      = 11U; // 64-bit number
    private enum REG_QWORD_LITTLE_ENDIAN        = 11U; // 64-bit number (same as REG_QWORD)

    private struct SECURITY_ATTRIBUTES {
        DWORD nLength;
        void* lpSecurityDescriptor;
        bool bInheritHandle;
    }

    private HRESULT RegOpenKeyExA(HKEY hKey, char* lpSubKey, DWORD ulOptions, DWORD samDesired, HKEY* phkResult);
    private HRESULT RegGetValueA(HKEY hkey, const(char)* lpSubKey, const(char)* lpValue, DWORD dwFlags, DWORD* pdwType, void* pvData, DWORD* pcbData);
    private HRESULT RegCreateKeyExA(HKEY hKey, const(char)* lpSubKey, DWORD Reserved, const(char)* lpClass, DWORD dwOptions, DWORD samDesired, const SECURITY_ATTRIBUTES* lpSecurityAttributes, HKEY* phkResult, DWORD* lpdwDisposition);
    private HRESULT RegSetKeyValueA(HKEY hKey, const(char)* lpSubKey, const(char)* lpValueName, DWORD dwType, const(void*) lpData, DWORD cbData);
    private HRESULT RegSetValueExA(HKEY hKey, const(char)* lpValueName, DWORD Reserved, DWORD dwType, const(void)* lpData, DWORD cbData);
    private HRESULT RegDeleteTreeA(HKEY hKey, const(char)* lpSubKey);
    private HRESULT RegFlushKey(HKEY hKey);
    private HRESULT RegCloseKey(HKEY hKey);

    extern(D) public string GetRegistryStringValue(string path, string name, bool useMachineKey) {
        DWORD strlen;
        RegGetValueA(cast(HKEY)(useMachineKey ? HKEY_LOCAL_MACHINE : HKEY_CURRENT_USER), cast(char*)path.toStringz(), cast(char*)name.toStringz(), REG_SZ, null, null, &strlen);
        char[] str = new char[strlen];
        HRESULT hr = RegGetValueA(cast(HKEY)(useMachineKey ? HKEY_LOCAL_MACHINE : HKEY_CURRENT_USER), cast(char*)path.toStringz(), cast(char*)name.toStringz(), REG_SZ, null, cast(void*)str.ptr, &strlen);
        return to!string(str[0..$-2]);
    }

    extern(D) public bool RegistryValueExists(string path, bool useMachineKey) {
        HKEY regKey;
        return RegOpenKeyExA(cast(HKEY)(useMachineKey ? HKEY_LOCAL_MACHINE : HKEY_CURRENT_USER), cast(char*)path.toStringz(), 0, KEY_READ, &regKey) == 0;
    }

    extern(D) public void SetRegistryUninstallData(string productId, string channelId, string title, string version_, string publisher, string baseDirectory, string installDirectory, int estimatedSize, bool useMachineKey) {
        HKEY regKey;
        string keyPath = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\" ~ productId ~ "-" ~ channelId;
        DWORD falseFlag = 1;
        RegCreateKeyExA(cast(HKEY)(useMachineKey ? HKEY_LOCAL_MACHINE : HKEY_CURRENT_USER), cast(char*)keyPath.toStringz(), 0, null, 0, KEY_WRITE, null, &regKey, null);
        string dip = buildNormalizedPath(installDirectory, "app.ico");
        if (exists(dip)) {
            RegSetValueExA(regKey, "DisplayIcon".toStringz(), 0, REG_SZ, cast(char*)dip.toStringz(), cast(int)dip.length+1);
        }
        RegSetValueExA(regKey, "DisplayName".toStringz(), 0, REG_SZ, cast(char*)title.toStringz(), cast(int)title.length+1);
        RegSetValueExA(regKey, "DisplayVersion".toStringz(), 0, REG_SZ, cast(char*)version_.toStringz(), cast(int)version_.length+1);
        RegSetValueExA(regKey, "NoModify".toStringz(), 0, REG_DWORD, &falseFlag, DWORD.sizeof);
        RegSetValueExA(regKey, "NoRepair".toStringz(), 0, REG_DWORD, &falseFlag, DWORD.sizeof);
        RegSetValueExA(regKey, "EstimatedSize".toStringz(), 0, REG_DWORD, &estimatedSize, DWORD.sizeof);
        SysTime now = Clock.currTime();
        string curDate = to!string(now.year) ~ to!string(to!int(now.month)) ~ to!string(now.day);
        RegSetValueExA(regKey, "InstallDate".toStringz(), 0, REG_SZ, cast(char*)curDate.toStringz(), cast(int)curDate.length+1);
        RegSetValueExA(regKey, "InstallLocation".toStringz(), 0, REG_SZ, cast(char*)baseDirectory.toStringz(), cast(int)baseDirectory.length+1);
        RegSetValueExA(regKey, "Publisher".toStringz(), 0, REG_SZ, cast(char*)publisher.toStringz(), cast(int)publisher.length+1);
        string qus = "\"" ~ buildNormalizedPath(baseDirectory, "uninstaller.exe") ~ "\"";
        RegSetValueExA(regKey, "QuietUninstallString".toStringz(), 0, REG_SZ, cast(char*)qus.toStringz(), cast(int)qus.length+1);
        string us = "\"" ~ buildNormalizedPath(baseDirectory, "uninstaller.exe") ~ "\"";
        RegSetValueExA(regKey, "UninstallString".toStringz(), 0, REG_SZ, cast(char*)us.toStringz(), cast(int)us.length+1);
        RegFlushKey(regKey);
        RegCloseKey(regKey);
    }

    extern(D) public void DeleteRegistryUninstallData(string productId, string channelId, bool useMachineKey) {
        string keyPath = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\" ~ productId ~ "-" ~ channelId;
        RegDeleteTreeA(cast(HKEY)(useMachineKey ? HKEY_LOCAL_MACHINE : HKEY_CURRENT_USER), cast(char*)keyPath.toStringz());
    }

    //System
    public struct GUID {
        ulong Data1;
        ushort Data2;
        ushort Data3;
        ubyte[8] Data4;
    }
    alias UUID = GUID;
    alias KNOWNFOLDERID = GUID;
    alias REFKNOWNFOLDERID = KNOWNFOLDERID*;

    // {F1B32785-6FBA-4FCF-9D55-7B8E7F157091}
    private static KNOWNFOLDERID FOLDERID_LocalAppData = {0xF1B32785, 0x6FBA, 0x4FCF, [0x9D, 0x55, 0x7B, 0x8E, 0x7F, 0x15, 0x70, 0x91]};
    // {B4BFCC3A-DB2C-424C-B029-7FE99A87C641}
    private static KNOWNFOLDERID FOLDERID_Desktop = {0xB4BFCC3A, 0xDB2C, 0x424C, [0xB0, 0x29, 0x7F, 0xE9, 0x9A, 0x87, 0xC6, 0x41]};
    // {A4115719-D62E-491D-AA7C-E74B8BE3B067}
    private static KNOWNFOLDERID FOLDERID_CommonStartMenu = {0xA4115719, 0xD62E, 0x491D, [0xAA, 0x7C, 0xE7, 0x4B, 0x8B, 0xE3, 0xB0, 0x67]};
    //	{625B53C3-AB48-4EC1-BA1F-A1EF4146FC19}
    private static KNOWNFOLDERID FOLDERID_StartMenu = {0x625B53C3, 0xAB48, 0x4EC1, [0xBA, 0x1F, 0xA1, 0xEF, 0x41, 0x46, 0xFC, 0x19]};

    private HRESULT SHGetKnownFolderPath(REFKNOWNFOLDERID rfid, DWORD dwFlags, HANDLE hToken, wchar **ppszPath);

    extern(D) public string GetCurrentUserLocalAppDataFolder() {
        return buildNormalizedPath(environment.get("LOCALAPPDATA"));
/*
        wchar *path;
        HRESULT hr = SHGetKnownFolderPath(&FOLDERID_LocalAppData, 0, null, &path);
        writeln(to!string(hr));
        auto result = fromStringz(path);
        return to!string(result.idup);
*/
    }

    extern(D) public string GetCurrentUserDesktopFolder() {
        return buildNormalizedPath(environment.get("USERPROFILE"), "Desktop");
/*
        wchar *path;
        HRESULT hr = SHGetKnownFolderPath(&FOLDERID_Desktop, 0, null, &path);
        writeln(to!string(hr));
        auto result = fromStringz(path);
        return to!string(result.idup);
*/
    }

    extern(D) public string GetAllUserStartMenuFolder() {
        return buildNormalizedPath(environment.get("PROGRAMDATA"), "Microsoft", "Windows", "Start Menu", "Programs");
/*
        wchar *path;
        HRESULT hr = SHGetKnownFolderPath(&FOLDERID_CommonStartMenu, 0, null, &path);
        writeln(to!string(hr));
        auto result = fromStringz(path);
        return to!string(result.idup);
*/
    }

    extern(D) public string GetCurrentUserStartMenuFolder() {
        return buildNormalizedPath(environment.get("APPDATA"), "Microsoft", "Windows", "Start Menu", "Programs");
/*
        wchar *path;
        HRESULT hr = SHGetKnownFolderPath(&FOLDERID_StartMenu, 0, null, &path);
        writeln(to!string(hr));
        auto result = fromStringz(path);
        return to!string(result.idup);
*/
    }

    //Shortcuts
    public enum SW_SHOWNORMAL = 1;
    public enum SW_SHOWMINIMIZED = 2;
    public enum SW_SHOWMAXIMIZED = 3;

    public enum STGM_READ = 0x00000000L;
    public enum STGM_WRITE = 0x00000001L;
    public enum STGM_READWRITE = 0x00000002L;

    // {00021401-0000-0000-C000-000000000046}
    private static CLSID CLSID_ShellLink = {0x00021401, 0x0000, 0x0000, [0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46]};

    extern(D) public void SetFileShortcut(string linkPath, string targetPath, string workingDirectory = null, string arguments = null, string description = null, int iconIndex = 0) {
        auto hr = CoInitialize(null);

        IShellLinkW pLink;
        hr = CoCreateInstance(&CLSID_ShellLink, null, CLSCTX_INPROC_SERVER, &IID_IShellLinkW, cast(void**)&pLink);
        if (hr == 0) {
            IPersistFile pFile;
            hr = pLink.QueryInterface(&IID_IPersistFile, cast(void**)&pFile);
            if (hr == 0) {
                if (exists(linkPath)) {
                    pFile.Load(linkPath.toUTF16z(), STGM_READWRITE);
                }
                pLink.SetPath(targetPath !is null ? targetPath.toUTF16z() : null);
                pLink.SetWorkingDirectory(workingDirectory !is null ? workingDirectory.toUTF16z() : null);
                pLink.SetArguments(arguments !is null ? arguments.toUTF16z() : null);
                pLink.SetDescription(description !is null ? description.toUTF16z() : null);
                pLink.SetIconLocation(iconIndex != int.min ? targetPath.toUTF16z() : null, iconIndex);
                pLink.SetShowCmd(SW_SHOWNORMAL);

                pFile.Save(linkPath.toUTF16z(), true);
                pFile.Release();
            } else {
                writeError("Failed to create instance of IPersistFile");
                writeln(hr);
            }
            pLink.Release();
        } else {
            writeError("Failed to create instance of IShellLinkW");
            writeln(hr);
        }

        CoUninitialize();
    }
}