module gridpack.commands.deploy;

import std.base64;
import std.conv;
import std.file;
import std.path;
import std.process;
import std.stdio;
import std.string;
import std.uuid;

import archive.tar;
import sdlang;

import ellipticbit.buildgrid.release;
import griddeploy.model;
import gridpack.options;
import gridpack.utils;

public immutable ubyte[] libzstd64 = cast(immutable(ubyte[]))import("win64/zstd.lib");
public immutable ubyte[] libzstd32 = cast(immutable(ubyte[]))import("win32/zstd.lib");

public void Deploy(DeployOptions options, string specPath) {
    string baseDirectory = buildNormalizedPath(tempDir(), randomUUID().toString());
    string outputDirectory = buildNormalizedPath(baseDirectory, "output");
    mkdirRecurse(outputDirectory);
    writeln("Build Output Directory: " ~ baseDirectory);
	writeln("Using Windows SDK: " ~ GetWindowsSdkToolsPath(options));
	writeln();
	
    scope(exit) {
        //Remove temp directory when finished;
        //rmdirRecurse(baseDirectory);
    }

    Package spec = new Package(parseFile(specPath), options.BaseDirectory, options.Version, options.PlatformId);

    //Write the spec file to the temp directory
    std.file.write(buildNormalizedPath(outputDirectory, "spec.sdl"), spec.toString());

    writeln("Building installer data package... ");
    BuildPackage(options, spec, baseDirectory, outputDirectory);
    writeln("Done.");

    write("Building uninstaller executable... ");
    BuildUninstaller(options, spec, baseDirectory, outputDirectory);
    writeln("Done.");

    write("Building installer executable... ");
    BuildInstaller(options, spec, baseDirectory, outputDirectory);
    writeln("Done.");

    if (!options.NoUpload) {
        Upload(options, spec, outputDirectory);
    }
}

private void BuildPackage(DeployOptions options, Package spec, string baseDirectory, string outputDirectory) {
    baseDirectory = buildNormalizedPath(baseDirectory);
    auto archive = new TarArchive();

    foreach(FileList fl; spec.Files) {
        foreach(string f; fl.Files) {
            string targetFile = f[options.BaseDirectory.length..$];

            auto file = new TarArchive.File(targetFile);
            file.data = cast(immutable(ubyte[]))std.file.read(f);
            archive.addFile(file);
        }
    }

    writeArchive(buildNormalizedPath(outputDirectory, "package.bin"), archive);
}

private void BuildInstaller(DeployOptions options, Package spec, string baseDirectory, string outputDirectory) {
    string buildDirectory = buildNormalizedPath(baseDirectory, "installer");
    string sourceDirectory = buildNormalizedPath(buildDirectory, "source");
    mkdirRecurse(sourceDirectory);
    string lib64path = buildNormalizedPath(buildDirectory, "libs", "win64");
    mkdirRecurse(lib64path);
    string lib32path = buildNormalizedPath(buildDirectory, "libs", "win32");
    mkdirRecurse(lib32path);

	string deploySourcePath = buildNormalizedPath(dirName(thisExePath()), "griddeploy");
	if (!exists(deploySourcePath)) deploySourcePath = options.DeploySourcePath;

    std.file.write(buildNormalizedPath(sourceDirectory, "app.d"), InstallerDFile.replace("<TenantId>", options.TenantId).replace("<ProductId>", options.DeploymentId).replace("<ChannelId>", options.ChannelId));
    std.file.write(buildNormalizedPath(buildDirectory, "dub.sdl"), InstallerDubFile.replace("<OutputDirectory>", outputDirectory.replace("\\", "\\\\")).replace("<GridDeployPath>", deploySourcePath.replace("\\", "\\\\")));
    std.file.write(buildNormalizedPath(lib64path, "zstd.lib"), libzstd64);
    std.file.write(buildNormalizedPath(lib32path, "zstd.lib"), libzstd32);

    string rcPath = buildNormalizedPath(GetWindowsSdkToolsPath(options), "rc.exe");
    string resourcePath = buildNormalizedPath(buildDirectory, "default.rc");
    if (exists(spec.Meta.IconPath)) {
        std.file.write(resourcePath, ResourceFile
            .replace("<IconPath>", "1 ICON \"" ~ spec.Meta.IconPath.replace("\\", "\\\\") ~ "\"")
            .replace("<Owners>", spec.Meta.Owners.join("; "))
            .replace("<Description>", spec.Meta.Description)
            .replace("<Version>", spec.Meta.Version)
            .replace("<Copyright>", spec.Meta.Copyright)
            .replace("<Name>", spec.Meta.Title));
    } else {
        std.file.write(resourcePath, ResourceFile
            .replace("<IconPath>", string.init)
            .replace("<Owners>", spec.Meta.Owners.join("; "))
            .replace("<Description>", spec.Meta.Description)
            .replace("<Version>", spec.Meta.Version)
            .replace("<Copyright>", spec.Meta.Copyright)
            .replace("<Name>", spec.Meta.Title));
    }
    auto rtr = execute([rcPath, "/nologo", resourcePath], null, Config.none, size_t.max, buildDirectory);
    if (rtr.status != 0) {
        writeError("Failed to build the installer resource file.");
        writeln(rtr.output);
        return;
    }

    auto result = execute(["dub", "build", "-b", "release"], null, Config.none, size_t.max, buildDirectory);
    if (result.status != 0) {
        writeError("Failed to build the installer.");
        writeln(result.output);
        return;
    }

    version(Windows) {
        string installerPath = buildNormalizedPath(buildDirectory, "installer.exe");
        if (spec.Install.AllUsers) {
            string mtPath = buildNormalizedPath(GetWindowsSdkToolsPath(options), "mt.exe");
            string manifestPath = buildNormalizedPath(buildDirectory, "app.manifest");
            std.file.write(manifestPath, AdministratorManifestFile.replace("<Version>", spec.Meta.Version).replace("<Name>", spec.Meta.Title));
            auto mtr = executeShell("\"" ~ mtPath ~ "\" -nologo -manifest app.manifest -outputresource:installer.exe;1", null, Config.none, size_t.max, buildDirectory);
            if (mtr.status != 0) {
                writeError("Failed to embed the security manifest file the installer.");
                writeln(mtr.output);
                return;
            }
        }
        copy(installerPath, buildNormalizedPath(outputDirectory, "installer.exe"));
    } else {
        copy(buildNormalizedPath(buildDirectory, "installer"), buildNormalizedPath(outputDirectory, "installer"));
    }
}

private void BuildUninstaller(DeployOptions options, Package spec, string baseDirectory, string outputDirectory) {
    string buildDirectory = buildNormalizedPath(baseDirectory, "uninstaller");
    string sourceDirectory = buildNormalizedPath(buildDirectory, "source");
    mkdirRecurse(sourceDirectory);
    string lib64path = buildNormalizedPath(buildDirectory, "libs", "win64");
    mkdirRecurse(lib64path);
    string lib32path = buildNormalizedPath(buildDirectory, "libs", "win32");
    mkdirRecurse(lib32path);

	string deploySourcePath = buildNormalizedPath(dirName(thisExePath()), "griddeploy");
	if (!exists(deploySourcePath)) deploySourcePath = options.DeploySourcePath;

    std.file.write(buildNormalizedPath(sourceDirectory, "app.d"), UninstallerDFile.replace("<TenantId>", options.TenantId).replace("<ProductId>", options.DeploymentId).replace("<ChannelId>", options.ChannelId));
    std.file.write(buildNormalizedPath(buildDirectory, "dub.sdl"), UninstallerDubFile.replace("<GridDeployPath>", deploySourcePath.replace("\\", "\\\\")));
    std.file.write(buildNormalizedPath(lib64path, "zstd.lib"), libzstd64);
    std.file.write(buildNormalizedPath(lib32path, "zstd.lib"), libzstd32);

    string rcPath = buildNormalizedPath(GetWindowsSdkToolsPath(options), "rc.exe");
    string resourcePath = buildNormalizedPath(buildDirectory, "default.rc");
    if (exists(spec.Meta.IconPath)) {
        std.file.write(resourcePath, ResourceFile
            .replace("<IconPath>", "1 ICON \"" ~ spec.Meta.IconPath.replace("\\", "\\\\") ~ "\"")
            .replace("<Owners>", spec.Meta.Owners.join("; "))
            .replace("<Description>", spec.Meta.Description)
            .replace("<Version>", spec.Meta.Version)
            .replace("<Copyright>", spec.Meta.Copyright)
            .replace("<Name>", spec.Meta.Title));
    } else {
        std.file.write(resourcePath, ResourceFile
            .replace("<IconPath>", string.init)
            .replace("<Owners>", spec.Meta.Owners.join("; "))
            .replace("<Description>", spec.Meta.Description)
            .replace("<Version>", spec.Meta.Version)
            .replace("<Copyright>", spec.Meta.Copyright)
            .replace("<Name>", spec.Meta.Title));
    }
    auto rtr = execute([rcPath, "/nologo", resourcePath], null, Config.none, size_t.max, buildDirectory);
    if (rtr.status != 0) {
        writeError("Failed to build the uninstaller.");
        writeln(rtr.output);
        return;
    }

    auto result = execute(["dub", "build", "-b", "release"], null, Config.none, size_t.max, buildDirectory);
    if (result.status != 0) {
        writeError("Failed to build the uninstaller.");
        writeln(result.output);
        return;
    }

    version(Windows) {
        string uninstallerPath = buildNormalizedPath(buildDirectory, "uninstaller.exe");
        if (spec.Install.AllUsers) {
            string mtPath = buildNormalizedPath(GetWindowsSdkToolsPath(options), "mt.exe");
            string manifestPath = buildNormalizedPath(buildDirectory, "app.manifest");
            std.file.write(manifestPath, AdministratorManifestFile.replace("<Version>", spec.Meta.Version).replace("<Name>", spec.Meta.Title));
            auto mtr = executeShell("\"" ~ mtPath ~ "\" -nologo -manifest app.manifest -outputresource:uninstaller.exe;1", null, Config.none, size_t.max, buildDirectory);
            if (mtr.status != 0) {
                writeError("Failed to embed the security manifest file the uninstaller.");
                writeln(mtr.output);
                return;
            }
        }
        copy(uninstallerPath, buildNormalizedPath(outputDirectory, "uninstaller.bin"));
    } else {
        copy(buildNormalizedPath(buildDirectory, "uninstaller"), buildNormalizedPath(outputDirectory, "uninstaller.bin"));
    }
}

private void Upload(DeployOptions options, Package spec, string outputDirectory) {
	auto rc = ReleaseClient();
	rc.Tenant = options.TenantId;
	rc.Deployment = options.DeploymentId;
	rc.Channel = options.ChannelId;
	rc.PushKey = options.PushKey;

	auto rm = Release();
	rm.PlatformId = options.PlatformId;
	rm.VersionId = options.Version;
	rm.Metadata = std.file.readText(buildNormalizedPath(outputDirectory, "spec.sdl"));
	rm.ReleaseNotes = spec.Meta.ReleaseNotesUrl;
	rm.IconUrl = spec.Meta.IconUrl;
	if (exists(spec.Meta.IconPath)) {
		rm.Icon = to!(ubyte[])(Base64.encode(cast(ubyte[])std.file.read(spec.Meta.IconPath)));
	}

    write("Uploading Metadata... ");
	auto rmr = pushMetadata(rc, rm);
	if(rmr.StatusCode != 200) {
		writeln("Failed. Metadata push returned: " ~ to!string(rmr.StatusCode) ~ " - " ~ rmr.Reason);
		return;
	}
	rm = rmr.Result;
    writeln("Done.");

    write("Uploading Package... ");
    version(Windows) {
		pushPackage(rc, rm.Id, buildNormalizedPath(outputDirectory, "installer.exe"));
    } else {
		pushPackage(rc, rm.Id, buildNormalizedPath(outputDirectory, "installer"));
    }
    writeln("Done.");
}

private immutable string InstallerDFile = q{
import griddeploy.options;
import griddeploy.commands.install;

public immutable string DefaultTenantId  = "<TenantId>";
public immutable string DefaultProductId = "<ProductId>";
public immutable string DefaultChannelId = "<ChannelId>";

public immutable string SpecData = import("spec.sdl");
public immutable string Uninstaller = import("uninstaller.bin");
public immutable string Package = import("package.bin");

int main(string[] args)
{
	Options opts = GetOptionsFromArgs(args[1..$], DefaultTenantId, DefaultProductId, DefaultChannelId);
	if (opts is null) {
		return 1;
	}

    Install(opts, cast(string)SpecData, cast(ubyte[])Package, cast(ubyte[])Uninstaller);

	return 0;
}
};

private immutable string UninstallerDFile = q{
import griddeploy.options;
import griddeploy.commands.uninstall;

public immutable string DefaultTenantId  = "<TenantId>";
public immutable string DefaultProductId = "<ProductId>";
public immutable string DefaultChannelId = "<ChannelId>";

int main(string[] args)
{
	Options opts = GetOptionsFromArgs(args[1..$], DefaultTenantId, DefaultProductId, DefaultChannelId);
	if (opts is null) {
		return 1;
	}

    Uninstall(opts);

	return 0;
}
};

private immutable string InstallerDubFile = `name "installer"
targetType "executable"
dependency "sdlang-d" version="~>0.10.6"
dependency "archive" version="~>0.7.0"
dependency "griddeploy" path="<GridDeployPath>"
subConfiguration "griddeploy" "installer"
sourceFiles "./default.res" platform="windows"
stringImportPaths "<OutputDirectory>"
lflags "/LIBPATH:$PACKAGE_DIR\\libs\\win64" platform="windows-x86_64"
lflags "/LIBPATH:$PACKAGE_DIR\\libs\\win32" platform="windows-x86"
`;

private immutable string UninstallerDubFile = `name "uninstaller"
targetType "executable"
dependency "sdlang-d" version="~>0.10.6"
dependency "archive" version="~>0.7.0"
dependency "griddeploy" path="<GridDeployPath>"
subConfiguration "griddeploy" "uninstaller"
sourceFiles "./default.res" platform="windows"
lflags "/LIBPATH:$PACKAGE_DIR\\libs\\win64" platform="windows-x86_64"
lflags "/LIBPATH:$PACKAGE_DIR\\libs\\win32" platform="windows-x86"
`;

private immutable string AdministratorManifestFile = `<?xml version="1.0" encoding="utf-8"?>
<asmv1:assembly manifestVersion="1.0" xmlns="urn:schemas-microsoft-com:asm.v1"
    xmlns:asmv1="urn:schemas-microsoft-com:asm.v1" 
    xmlns:asmv2="urn:schemas-microsoft-com:asm.v2" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <assemblyIdentity version="<Version>" name="<Name>" />
    <trustInfo xmlns="urn:schemas-microsoft-com:asm.v2">
        <security>
            <requestedPrivileges xmlns="urn:schemas-microsoft-com:asm.v3">
                <requestedExecutionLevel level="requireAdministrator" uiAccess="false" />
            </requestedPrivileges>
        </security>
    </trustInfo>
</asmv1:assembly>
`;

public immutable string ResourceFile = `<IconPath>
2 VERSIONINFO
BEGIN
  BLOCK "StringFileInfo"
  BEGIN
    BLOCK "040904E4"
    BEGIN
      VALUE "CompanyName", "<Owners>"
      VALUE "FileDescription", "<Description>"
      VALUE "FileVersion", "<Version>"
      VALUE "LegalCopyright", "<Copyright>"
      VALUE "ProductName", "<Name>"
      VALUE "ProductVersion", "<Version>"
    END
  END
  BLOCK "VarFileInfo"0
  BEGIN
    VALUE "Translation", 0x409, 1252
  END
END`;
