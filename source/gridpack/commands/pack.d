module gridpack.commands.pack;
version(Disable):

import std.array;
import std.algorithm.searching;
import std.file;
import std.path;
import std.stdio;
import std.string;

import archive.tar;

import gridpack.model;
import gridpack.options;

public void createPackage(Options opts, Package pkg) {
    string pkgName = pkg.Meta.Id ~ "." ~ pkg.Meta.Version ~ ".gridpkg";
    string pkgSpecName = pkg.Meta.Id ~ "." ~ pkg.Meta.Version ~ ".gpkspec";
    string tempPath = tempDir();
    const string pkgPath = buildNormalizedPath(opts.OutputDirectory, pkgName);

    auto gridpkg = new TarArchive();
    auto specfile = new TarArchive.File(pkgSpecName);
    specfile.data = cast(immutable(ubyte[]))pkg.toString();
    gridpkg.addFile(specfile);

    foreach(FileList fl; pkg.Files) {
        string targetPath = fl.Target.toPath();
        foreach(string f; fl.Files) {
            string sourceFile = buildNormalizedPath(opts.BaseDirectory, f);
            string fileName = baseName(sourceFile);
            string targetFile = buildNormalizedPath(targetPath, fileName);

            auto file = new TarArchive.File(targetFile);
            file.data = cast(immutable(ubyte[]))std.file.read(sourceFile);
            file.name = f;
            gridpkg.addFile(file);
        }
    }

    //Run ZSTD compression on resulting tar file.
    std.file.write(pkgPath, compress(cast(ubyte[]) gridpkg.serialize(), 22));
}

public void extractPackage(string packagePath, string outputFolder) {
    //Decompress package
    auto uncompressor = new Decompressor();
    ubyte[] uncompressed;
    File pkgFile = File(packagePath, "rb");
    scope(exit) pkgFile.close();
    foreach (ubyte[] buffer; pkgFile.byChunk(33_554_432))
    {
        uncompressed ~= uncompressor.decompress(buffer);
    }

    //Extract individual files to the outputFolder
    auto pkgArchive = new TarArchive(uncompressed);
    foreach(file; pkgArchive.files()) {
        writeFile(buildNormalizedPath(outputFolder, file.path), file.data);
    }
}

private void writeFile(string filePath, immutable(ubyte)[] data) {
    File pkgFile = File(filePath, "wb");
    scope(exit) pkgFile.close();

    pkgFile.write(data);
    pkgFile.flush();
}
