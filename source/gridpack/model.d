module gridpack.model;

import sdlang;

import std.algorithm;
import std.array;
import std.conv;
import std.file;
import std.path;
import std.stdio;
import std.string;
import std.typecons;

import gridpack.options;

public enum PackageType
{
    PACKAGE,
    DEPLOYMENT
}

public enum DependencyType
{
    PREDEFINED,
    PACKAGE,
    POWERSHELL,
    BATCH,
    SHELL,
}

public enum DependencyPredefined
{
    NET40,
    NET45,
    NET451,
    NET452,
    NET46,
    NET461,
    NET462,
    NET47,
    NET471,
    NET472,
	NET48,
    NETCORE10,
    NETCORE11,
    NETCORE20,
    NETCORE21,
    NETCORE30,
    NETCORE31,
    NET50,
    VCRT2008SP1,
    VCRT2010SP1,
    VCRT2012U4,
    VCRT2013,
    VCRT2015U3,
    VCRT2017,
}

public enum TargetType
{
    BIN,
    TOOLS,
    SOURCE,
    INCLUDE,
    CONTENT,
    DOCS,
    ROOT
}

public enum TargetLanguage
{
    ANY,
    DLANG,
    CPP,
    NET
}

public enum TargetToolchain
{
    ANY,
    DLANG,
    DMD,
    LDC,
    GDC,
    CPP,
    CLANG5,
    GCC8,
    MSVC15,
    NET,
    NET45,
    NET451,
    NET452,
    NET46,
    NET461,
    NET462,
    NET47,
    NET471,
    NET472,
    NET48,
    NETCOREAPP20,
    NETSTANDARD20,
    NETCOREAPP21,
    NETSTANDARD21,
    NETCOREAPP30,
    NETCOREAPP31,
	NET50,
}

public enum TargetArchitecture
{
    X86,
    X64,
    ARM32,
    ARM64,
}

public class Metadata
{
    public string Id;
    public PackageType Type;
    public string Title;
    public string Version;
    public string[] Authors;
    public string[] Owners;
    public string Copyright;
    public string IconPath;
    public string IconUrl;
    public string ProjectUrl;
    public string ReleaseNotesUrl;
    public string SupportUrl;
    public string LicenseUrl;
    public bool RequireLicenseAcceptance;
    public string Summary;
    public string Description;
    public string[] Tags;

    public this (Tag root, string versionOverride)
    {
        this.Id = root.expectTagValue!string("id");
        this.Type = to!PackageType(root.expectTagValue!string("type").toUpper());
        this.Title = root.expectTagValue!string("title");
        if (versionOverride !is null && versionOverride.strip() != string.init) {
            this.Version = versionOverride;
        } else {
            this.Version = root.expectTagValue!string("version");
        }
        string al = root.getTagValue!string("authors", null);
        if (al !is null)
        {
            string[] temp = string[].init;
            al.split(";").each!((string s) => { if (s.strip() != "") temp ~= s.strip; });
            this.Authors = temp;
        } else {
            this.Authors = null;
        }
        string ol = root.getTagValue!string("owners", null);
        if (ol !is null)
        {
            string[] temp = string[].init;
            ol.split(";").each!((string s) => { if (s.strip() != "") temp ~= s.strip; });
            this.Owners = temp;
        } else {
            this.Owners = null;
        }
        this.Copyright = root.getTagValue!string("copyright", null);
        this.IconPath = root.getTagValue!string("iconPath", null);
        this.IconUrl = root.getTagValue!string("iconUrl", null);
        this.ProjectUrl = root.getTagValue!string("projectUrl", null);
        this.ReleaseNotesUrl = root.getTagValue!string("releaseNotesUrl", null);
        this.SupportUrl = root.getTagValue!string("supportUrl", null);
        this.LicenseUrl = root.getTagValue!string("licenseUrl", null);
        this.RequireLicenseAcceptance = root.getTagValue!bool("requireLicenseAcceptance", false);
        this.Summary = root.getTagValue!string("summary", null);
        this.Description = root.getTagValue!string("description", null);
        string tl = root.getTagValue!string("tags", null);
        if (tl !is null)
        {
            string[] temp = string[].init;
            tl.split(";").each!((string s) => { if (s.strip() != "") temp ~= s.strip; });
            this.Tags = temp;
        } else {
            this.Tags = null;
        }
    }

    public Tag toTag() {
        Tag root = new Tag(null, "metadata");

        root.add(new Tag(null, "id", [Value(this.Id)]));
        root.add(new Tag(null, "type", [Value(to!string(this.Type))]));
        root.add(new Tag(null, "title", [Value(this.Title)]));
        root.add(new Tag(null, "version", [Value(this.Version)]));

        if (this.Authors.length > 0) {
            string str = string.init;
            foreach(string s; this.Authors) {
                str ~= s ~ ";";
            }
            root.add(new Tag(null, "authors", [Value(str)]));
        }
        if (this.Owners.length > 0) {
            string str = string.init;
            foreach(string s; this.Owners) {
                str ~= s ~ ";";
            }
            root.add(new Tag(null, "owners", [Value(str)]));
        }
        if (this.Copyright !is null) root.add(new Tag(null, "copyright", [Value(this.Copyright)]));
        if (this.IconUrl !is null) root.add(new Tag(null, "iconUrl", [Value(this.IconUrl)]));
        if (this.ProjectUrl !is null) root.add(new Tag(null, "projectUrl", [Value(this.ProjectUrl)]));
        if (this.ReleaseNotesUrl !is null) root.add(new Tag(null, "releaseNotesUrl", [Value(this.ReleaseNotesUrl)]));
        if (this.SupportUrl !is null) root.add(new Tag(null, "supportUrl", [Value(this.SupportUrl)]));
        if (this.LicenseUrl !is null) root.add(new Tag(null, "licenseUrl", [Value(this.LicenseUrl)]));
        root.add(new Tag(null, "requireLicenseAcceptance", [Value(cast(bool)this.RequireLicenseAcceptance)]));
        if (this.Summary !is null) root.add(new Tag(null, "summary", [Value(this.Summary)]));
        if (this.Description !is null) root.add(new Tag(null, "description", [Value(this.Description)]));
        if (this.Tags.length > 0) {
            string str = string.init;
            foreach(string s; this.Tags) {
                str ~= s ~ ";";
            }
            root.add(new Tag(null, "tags", [Value(str)]));
        }

        return root;
    }
}

public class FileTarget
{
    public TargetType Type;
    public TargetLanguage Language;
    public TargetToolchain Toolchain;
    public Nullable!TargetArchitecture Architecture;
    public string Path;

    public this(string targetPath) {
        string[] pathParts = targetPath.toUpper().split("\\");

        this.Type = to!TargetType(pathParts[0]);
        if (this.Type != TargetType.ROOT) {
            this.Language = to!TargetLanguage(pathParts[1]);
            this.Toolchain = to!TargetToolchain(pathParts[2]);
            if (pathParts.length > 3 && (pathParts[3] == "X86" || pathParts[3] == "X64" || pathParts[3] == "ARM32" || pathParts[3] == "ARM64")) {
                TargetArchitecture arch = to!TargetArchitecture(pathParts[2]);
                this.Architecture = Nullable!TargetArchitecture(arch);
                this.Path = buildNormalizedPath(pathParts[4..$]);
            } else if (pathParts.length > 3) {
                this.Architecture = Nullable!TargetArchitecture();
                this.Path = buildNormalizedPath(pathParts[3..$]);
            } else {
                this.Architecture = Nullable!TargetArchitecture();
                this.Path = string.init;
            }
        } else {
            this.Path = buildNormalizedPath(pathParts[1..$]);
        }
    }

    public override string toString() {
        if (this.Architecture.isNull) {
            return buildNormalizedPath(to!string(this.Type), to!string(this.Language), to!string(this.Toolchain), Path).toLower();
        } else {
            return buildNormalizedPath(to!string(this.Type), to!string(this.Language), to!string(this.Toolchain), to!string(this.Architecture.get()), Path).toLower();
        }
    }

    public string toPath() {
        string path = string.init;

        if (this.Type != TargetType.ROOT) {
            path = buildNormalizedPath(path, to!string(this.Type).toLower());
        }

        if (this.Language != TargetLanguage.ANY) {
            path = buildNormalizedPath(path, to!string(this.Language).toLower());
        }

        if (this.Toolchain != TargetToolchain.ANY) {
            path = buildNormalizedPath(path, to!string(this.Toolchain).toLower());
        }

        if (!this.Architecture.isNull) {
            path = buildNormalizedPath(path, to!string(this.Architecture).toLower());
        }

        path = buildNormalizedPath(path, this.Path);

        return path;
    }
}

public class FileList
{
    public string[] Files;
    public FileTarget Target;

    public this(Tag root, string baseDirectory)
    {
        this.Target = new FileTarget(root.expectAttribute!string("target"));

        auto path = buildNormalizedPath(baseDirectory, root.expectAttribute!string("path"));
        auto pattern = root.getAttribute!string("pattern", null);

        if (pattern is null && exists(path) && isFile(path)) {
            Files ~= path;
        }
        else if (pattern !is null && exists(path) && isDir(path)) {
            string[] exclusionList = GetExclusionList(root);
            string[] temp = string[].init;
            pattern.split(";").each!((string s) => { if (s.strip() != "") temp ~= s.strip(); });

            DirEntry[] tfl = DirEntry[].init;
            foreach(string t; temp) {
                tfl ~= dirEntries(path, t, SpanMode.shallow).array;
            }
            DirEntry[] files = DirEntry[].init;
            foreach(DirEntry d; tfl) {
                foreach(string e; exclusionList) {
                    if (baseName(d.name) != e)
                        files ~= d;
                }
            }

            string[] fl = string[].init;
            files.each!((DirEntry de) => { if (de.isFile) fl ~= de.name; });
            this.Files = fl;
        }
    }

    private string[] GetExclusionList(Tag root)
    {
        if (root.all.tags.length == 0)
        {
            return null;
        }

        string[] tel = string[].init;

        foreach (Tag e; root.all.tags)
        {
            if (e.name != "exclude") {
                continue;
            }

            tel ~= e.expectValue!string();
        }

        return tel;
    }
}

public class Script {
    public string Source;
    public string Arguments;
    public string Shell;
    public bool RequiresElevation;
    public int ExpectedExitCode;
    public bool PostInstall;

    public this(Tag root) {
        this.Source = root.values[0].get!string();
        this.Arguments = root.getAttribute!string("arguments", null);
        this.Shell = root.getAttribute!string("shell", null);
        this.RequiresElevation = root.getAttribute!bool("requiresElevation", false);
        this.ExpectedExitCode = root.getAttribute!int("expectedExitCode", 0);
        this.PostInstall = root.getAttribute!bool("postInstall", false);
    }

    public Tag toTag() {
        Tag root = new Tag(null, "script", [Value(this.Source)]);
        root.add(new Attribute("arguments", Value(this.Arguments)));
        root.add(new Attribute("shell", Value(this.Shell)));
        root.add(new Attribute("expectedExitCode", Value(cast(int)this.ExpectedExitCode)));
        root.add(new Attribute("requiresElevation", Value(cast(bool)this.RequiresElevation)));
        root.add(new Attribute("postInstall", Value(cast(bool)this.PostInstall)));
        return root;
    }
}

public class Package
{
    public Metadata Meta;

    public FileList[] Files;

    public Script[] Scripts;

    public this(Tag root, Options opts) {
        this.Meta = new Metadata(root.expectTag("metadata"), opts.Version);
        FileList[] tfl;
        Script[] tsl;
        foreach(Tag f; root.all.tags) {
            if (f.name == "file") tfl ~= new FileList(f, opts.BaseDirectory);
            if (f.name == "script") tsl ~= new Script(f);
        }
        this.Files = tfl;
        this.Scripts = tsl;
    }

    public override string toString() {
        Tag root = new Tag();

        root.add(Meta.toTag());

        if (this.Scripts.length > 0) {
            foreach(Script s; this.Scripts) {
                root.add(s.toTag());
            }
        }

        return root.toSDLDocument();
    }
}

public Package loadPackageSpec(string packageSpecPath, Options opts) {
    auto root = parseFile(packageSpecPath);
    return new Package(root, opts);
}
