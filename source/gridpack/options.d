module gridpack.options;

import std.array;
import std.algorithm.searching;
import std.file;
import std.path;
import std.stdio;
import std.string;

import gridpack.apis;

public abstract class Options
{
    public string BaseDirectory;
    public string OutputDirectory;
    public bool ExcludeEmptyDirectories;
    public string Version;
    public string PlatformId;

    private this(string[] args) {
        string baseDir = getcwd();
        string outDir = getcwd();
        bool excludeDirs = false;
        string version_ = string.init;
        string platformId = GetSystemArch();

        if (args.length > 0) {
            foreach (string arg; args)
            {
                if (arg is null || arg.strip() == "") continue;

                if (arg.startsWith("--baseDirectory=") ||
                    arg.startsWith("/BaseDirectory=") ||
                    arg.startsWith("-bd="))
                {
                    baseDir = arg
                        .replace("--baseDirectory=", "")
                        .replace("/BaseDirectory=", "")
                        .replace("-bd=", "");

                    if (baseDir.startsWith("\"") && baseDir.endsWith("\""))
                    {
                        baseDir = baseDir[1..$-2];
                    }

                    baseDir = asAbsolutePath(baseDir).array;
                }

                else if (arg.startsWith("--outputDirectory=") ||
                    arg.startsWith("/OutputDirectory=") ||
                    arg.startsWith("-od="))
                {
                    outDir = arg
                        .replace("--outputDirectory=", "")
                        .replace("/OutputDirectory=", "")
                        .replace("-od=", "");

                    if (outDir.startsWith("\"") && outDir.endsWith("\""))
                    {
                        outDir = outDir[1..$-2];
                    }

                    outDir = asAbsolutePath(outDir).array;
                }

                else if (arg.startsWith("--excludeEmptyDirectories") ||
                    arg.startsWith("/ExcludeEmptyDirectories") ||
                    arg.startsWith("-eed"))
                {
                    excludeDirs = true;
                }

                else if (arg.startsWith("--version=") ||
                    arg.startsWith("/Version=") ||
                    arg.startsWith("-v="))
                {
                    version_ = arg
                        .replace("--version=", "")
                        .replace("/Version=", "")
                        .replace("-v=", "");

                    if (version_.startsWith("\"") && version_.endsWith("\""))
                    {
                        version_ = version_[1..$-2];
                    }
                }

                else if (arg.startsWith("--platformId=") ||
                    arg.startsWith("/PlatformId=") ||
                    arg.startsWith("-pid="))
                {
                    platformId = arg
                        .replace("--platformId=", "")
                        .replace("/PlatformId=", "")
                        .replace("-pid=", "");

                    if (platformId.startsWith("\"") && platformId.endsWith("\""))
                    {
                        platformId = platformId[1..$-2];
                    }
                }
            }
        }

        this.BaseDirectory = baseDir;
        this.OutputDirectory = outDir;
        this.ExcludeEmptyDirectories = excludeDirs;
        this.Version = version_;
        this.PlatformId = platformId;
    }
}

public class DeployOptions : Options
{
    public string TenantId;
    public string DeploymentId;
    public string ChannelId;
    public string PushKey;
    public bool NoUpload;
	public string DeploySourcePath;

    this(string[] args) {
        super(args);

        string tenantId = string.init;
        string deploymentId = string.init;
        string channelId = string.init;
        string pushKey = string.init;
        bool noUpload = false;
		string dsp = string.init;

        if (args.length > 0) {
            foreach (string arg; args)
            {
                if (arg is null || arg.strip() == "") continue;

                if (arg.startsWith("--tenantId=") ||
                    arg.startsWith("/TenantId=") ||
                    arg.startsWith("-tid="))
                {
                    tenantId = arg
                        .replace("--tenantId=", "")
                        .replace("/TenantId=", "")
                        .replace("-tid=", "");

                    if (tenantId.startsWith("\"") && tenantId.endsWith("\""))
                    {
                        tenantId = tenantId[1..$-2];
                    }
                }

                else if (arg.startsWith("--deploymentId=") ||
                    arg.startsWith("/DeploymentId=") ||
                    arg.startsWith("-did="))
                {
                    deploymentId = arg
                        .replace("--deploymentId=", "")
                        .replace("/DeploymentId=", "")
                        .replace("-did=", "");

                    if (deploymentId.startsWith("\"") && deploymentId.endsWith("\""))
                    {
                        deploymentId = deploymentId[1..$-2];
                    }
                }

                else if (arg.startsWith("--channelId=") ||
                    arg.startsWith("/ChannelId=") ||
                    arg.startsWith("-cid="))
                {
                    channelId = arg
                        .replace("--channelId=", "")
                        .replace("/ChannelId=", "")
                        .replace("-cid=", "");

                    if (channelId.startsWith("\"") && channelId.endsWith("\""))
                    {
                        channelId = channelId[1..$-2];
                    }
                }

                else if (arg.startsWith("--pushKey=") ||
                    arg.startsWith("/PushKey=") ||
                    arg.startsWith("-pk="))
                {
                    pushKey = arg
                        .replace("--pushKey=", "")
                        .replace("/PushKey=", "")
                        .replace("-pk=", "");

                    if (pushKey.startsWith("\"") && pushKey.endsWith("\""))
                    {
                        pushKey = pushKey[1..$-2];
                    }
                }

                else if (arg.startsWith("--deploySourcePath=") ||
                    arg.startsWith("/DeploySourcePath=") ||
                    arg.startsWith("-dsp="))
                {
                    dsp = arg
                        .replace("--deploySourcePath=", "")
                        .replace("/DeploySourcePath=", "")
                        .replace("-dsp=", "");

                    if (dsp.startsWith("\"") && dsp.endsWith("\""))
                    {
                        dsp = dsp[1..$-2];
                    }
                }

                else if (arg.startsWith("--noUpload") ||
                    arg.startsWith("/NoUpload") ||
                    arg.startsWith("-no"))
                {
                    noUpload = true;
                }
            }
        }

        this.TenantId = tenantId;
        this.DeploymentId = deploymentId;
        this.ChannelId = channelId;
        this.PushKey = pushKey;
		this.DeploySourcePath = dsp;
        this.NoUpload = noUpload;
    }

}

public string GetSystemArch() {
    version(Windows) {
        string arch = GetRegistryStringValue("SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment", "PROCESSOR_ARCHITECTURE", true);
        if (arch.toLower() == "x86".toLower()) return "windows-x86";
        else if (arch.toLower() == "AMD64".toLower()) return "windows-x64";
    }
    version(linux) {
        auto uname = executeShell("uname -m"); 
        string arch = uname.output.strip();
        if (arch.toLower() == "i386".toLower() || arch.toLower() == "i586".toLower() || arch.toLower() == "i686".toLower()) return "linux-x86";
        else if (arch.toLower() == "x86_64".toLower()) return "linux-x64";
    }
/*
    version(OSX) {
        return "macos-x64";
    }
*/
    else {
        static assert("Unsupported platform.");
    }
    return "any";
}
