module gridpack.utils;

import std.algorithm;
import std.file;
import std.range;
import std.path;
import std.process;
import std.stdio;
import std.string;

import archive.tar;
import zstd;
import zstd.c.zstd;

import gridpack.options;

public __gshared int errorCount;

public void writeError(string message) {
    errorCount++;
    writefln("ERROR: %s", message);
}

public void writeArchive(string path, TarArchive archive) {
    write("\tCompressing Package... ");
    auto archiveBytes = archive.serialize();
    auto compressed = compress(archiveBytes, 22);
    writeln("Done.");

    write("\tTesting Package Decompression... ");
    auto decompressed = uncompress(compressed);
    assert(archiveBytes.length==decompressed.length);
    writeln("Done.");

    write("\tWriting package file... ");
    std.file.write(path, compressed);
    writeln("Done.");
}

public string GetWindowsSdkToolsPath(Options options) {
    string installPath = environment.get("SystemDrive");
    if (GetSystemArch().toLower() == "windows-x86") {
		string win10path = buildNormalizedPath(installPath, "Program Files", "Windows Kits", "10", "bin");
		string win11path = buildNormalizedPath(installPath, "Program Files", "Windows Kits", "11", "bin");
		if (exists(win11path)) {
			auto folders = dirEntries(win11path, "11.*", SpanMode.shallow).array;
			if (folders.length == 0) {
				writeError("Unable to locate any Windows 11 SDK installations.");
				return null;
			}
			auto sfl = folders.sort!((a, b) => a.name < b.name);

			installPath = buildNormalizedPath(win11path, sfl[sfl.length-1], "x86");
		} else {
			auto folders = dirEntries(win10path, "10.*", SpanMode.shallow).array;
			if (folders.length == 0) {
				writeError("Unable to locate any Windows 11 SDK installations.");
				return null;
			}
			auto sfl = folders.sort!((a, b) => a.name < b.name);

			installPath = buildNormalizedPath(win10path, sfl[sfl.length-1], "x86");
		}
    }
    if (GetSystemArch().toLower() == "windows-x64") {
		string win10path = buildNormalizedPath(installPath, "Program Files (x86)", "Windows Kits", "10", "bin");
		string win11path = buildNormalizedPath(installPath, "Program Files (x86)", "Windows Kits", "11", "bin");
		if (exists(win11path)) {
			auto folders = dirEntries(win11path, "11.*", SpanMode.shallow).array;
			if (folders.length == 0) {
				writeError("Unable to locate any Windows 11 SDK installations.");
				return null;
			}
			auto sfl = folders.sort!((a, b) => a.name < b.name);

			installPath = buildNormalizedPath(win11path, sfl[sfl.length-1], "x64");
		} else {
			auto folders = dirEntries(win10path, "10.*", SpanMode.shallow).array;
			if (folders.length == 0) {
				writeError("Unable to locate any Windows 11 SDK installations.");
				return null;
			}
			auto sfl = folders.sort!((a, b) => a.name < b.name);

			installPath = buildNormalizedPath(win10path, sfl[sfl.length-1], "x64");
		}
    }
    return installPath;
}
